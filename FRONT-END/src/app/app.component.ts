import { Component, OnInit } from '@angular/core';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { StudentService } from './student.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  title = 'Front';
  dataUpload = [];
  allStudents = [];
  student = {
    name: '',
    lastmane: '',
    grade: ''
  }
  private customStatus = {
    error: 404,
    success: 200
  };

  private options: any = {
    url: 'http://localhost:3000/update-csv',
    method: 'POST',
    autoUpload: false,
  };
  public uploader: FileUploader;

  constructor(private studentService: StudentService){}

  ngOnInit(){
    this.uploader = new FileUploader(this.options);
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      // console.log('Uploaded:', item, status, response);
      this.dataUpload = response;
    };
    this.showAllStudents();
  }

  showAllStudents() {
    this.studentService.getUsersList()
    .subscribe(students => {
      console.log('all students: ', students);
      this.allStudents = students;
    })
  }
}
