import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const url = `http://localhost:3000/`;

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private http:HttpClient) { }

  getUsersList(): Observable<any> {
    return this.http.get(`${ url }students`);
  }
}
