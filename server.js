var express = require('express');
var app = express();
var bodyParser = require('body-parser');
const csv = require('fast-csv');
var multer = require('multer');
const fs = require('fs');
const cors = require('cors');


const upload = multer({ dest: 'files' });
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

let corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200
};

let students = [];
let counter = 0;

app.use(cors(corsOptions));
app.get('/students', function (req, res) {
    res.status(200).send(students);
  });

  //Add new student
app.post('/students', function (req, res) {
    // console.log('student:', req.body);
    let newStudent = {};
    counter += 1;
    newStudent.id = counter;
    newStudent.name = req.body.name;
    newStudent.lastname = req.body.lastname;
    students.push(newStudent);
    console.log('array:', students);
    res.status(200).send(newStudent);
});

//Find student by id
app.get('/students/:id', function (req, res) {
  console.log('student:', req.params);
  let idStudent = req.params.id;
  let foundStudent = students.find(student =>{
    return student.id === +idStudent;
  })
  if(foundStudent) {
    return res.status(200).send(foundStudent);
  }
  return res.status(400).send({
    status: false,
    message: 'not found'
  });
});


//Delete student by id
app.delete('/students/:id', function (req,res){

  console.log(students);
  console.log('req params:', req.params);
  let indexId = req.params.id;
  const indexStudent = students.indexOf(student => {
    return student.id = indexId;
  });

  if(!indexStudent){
    return res.status(400).send({
      status: false,
      message: 'doesnt exist'
    });
  }
  students.splice(indexStudent,1);
  console.log('studen:', students);
  return res.status(200).send({
    status: true,
    message: 'deleted'
  })
});

app.post('/update-csv', upload.single('file'), function (req, res) {
  console.log('updating',req.body);
  const fileRows = [];

  // open uploaded file
  csv.fromPath(req.file.path)
    .on('data', function (data) {
      fileRows.push(data); // push each row
    })
    .on('end', function () {
      // console.log('my rows:', fileRows)
      fs.unlinkSync(req.file.path);   // remove temp file
      //process "fileRows" and respond
      //tranformar a array de students
      students = students.concat(fileRows);
      console.log('aaaaaa', fileRows);
      return res.status(200).send({
        status: true,
        message: 'file uploaded',
        data: fileRows
      });
    });
});


  app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
  });

  